pragma solidity ^0.5.0;

contract Election {
	//store candidate 
	//Read candidate 
	


	//Model a candidate 
	
	struct Candidate{
		uint id;
		string name;
		uint voteCount;
	}

	//store a canddate
	//fetch a candidate

	mapping(uint => Candidate) public candidates;

	//store candiate count  
	
	uint public candidatesCount;

	string public candidate;


	constructor() public {
		addCandidate("Candidate 1");
		addCandidate("Candidate 2");	
	}	
	// private so that no one from outside ca access .  
	function addCandidate (string memory _name) private 
	{
		candidatesCount ++;
		candidates[candidatesCount] = Candidate(candidatesCount, _name, 0);
	}
}